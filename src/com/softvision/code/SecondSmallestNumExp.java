package com.softvision.code;

public class SecondSmallestNumExp {
	private int secondSmallestNum(int[] arr) {
		int minA = Integer.MAX_VALUE;
		int minB = Integer.MAX_VALUE;

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] < minA) {
				minB = minA;
				minA = arr[i];
			} else if (arr[i] < minB && arr[i] != minA) {
				minB = arr[i];
			}
		}

		return minB;
	}

	public static void main(String[] args) {
		int[] arr = new int[]{2, 12333, 21, 22, 2, 1, 12, 13, 1, 10, 34, 1, 2, 353, 23, 56, 2, 54, 332, 32, 64, 2314};

		SecondSmallestNumExp s = new SecondSmallestNumExp();

		int r = s.secondSmallestNum(arr);
		System.out.println("The second smallest number is " + r);
	}
}
