package com.softvision.code;

import com.softvision.helpers.ArrayTools;

public class BigONotation {

	public static void main(String[] args) {
		ArrayTools at = new ArrayTools();
		BigONotation bo = new BigONotation();
		int[] arr1 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1};
		int[] arr2 = new int[]{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		int[] arr3 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		boolean r1 = bo.findNeedle(arr1);

		int[] arr = at.random(100,10);
		bo.printAllNumbersThenAllPairSums(arr);

	}

	private boolean findNeedle(int[] haystack) {
		for (int n : haystack) {
			if (n == 1) {
				return true;
			}
		}
		return false;
	}

	private void printAllNumbersThenAllPairSums(int[] numbers) {
		// O(n)
		System.out.println("these are the numbers:");
		for (int number : numbers) {
			System.out.println(number);
		}

		// O(n2)
		System.out.println("and these are their sums:");
		for (int firstNumber : numbers) {
			for (int secondNumber : numbers) {
				System.out.println(firstNumber + secondNumber);
			}
		}
	}
}
