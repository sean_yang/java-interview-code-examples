package com.softvision.code;

import com.softvision.helpers.ArrayTools;

import java.util.Arrays;
import java.time.Instant;

public class SortingAlgorithms {

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/*
	Selection Sort algorithm sorts an array by repeatedly finding the minimum element (considering ascending order)
	from unsorted part and putting it at the beginning. The algorithm maintains two sub-arrays in a given array.

	1) The sub-array which is already sorted.
	2) Remaining sub-array which is unsorted.

	In every iteration of selection sort, the minimum element (considering ascending order) from the unsorted sub-array
	is picked and moved to the sorted sub-array.

	Time Complexity: O(n2) as there are two nested loops.
	*/

	private void selectionSort(int[] arr)
	{
		int n = arr.length;
		for (int i = 0; i < n-1; i++) {
			// Find the minimum element in unsorted array
			int min_idx = i;
			for (int j = i+1; j < n; j++) {
				if (arr[j] < arr[min_idx])
					min_idx = j;
			}

			// Swap the found minimum element with the first
			// element
			int temp = arr[min_idx];
			arr[min_idx] = arr[i];
			arr[i] = temp;
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/*
	Bubble Sort is the simplest sorting algorithm that works by repeatedly
	swapping the adjacent elements if they are in wrong order.

	Worst and Average Case Time Complexity: O(n*n). Worst case occurs when array is reverse sorted.
	Best Case Time Complexity: O(n). Best case occurs when array is already sorted.
	*/

	private void bubbleSort(int[] arr) {
		int n = arr.length;
		for (int i = 0; i < (n - 1); i++) {
			for (int j = 0; j < ((n - i) - 1); j++) {
				if (arr[j] > arr[j + 1]) {
					int tmp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = tmp;
				}
			}
		}
	}

	/*
	Recursive Bubble Sort has no performance/implementation advantages,
	but can be a good question to check one’s understanding of Bubble
	Sort and recursion.

	If we take a closer look at Bubble Sort algorithm, we can notice
	that in first pass, we move largest element to end (Assuming sorting
	in increasing order). In second pass, we move second largest element
	to second last position and so on.

	*/
	private void recBubbleSort(int[] arr, int n) {
		if (n == 1) { return; }

		// One pass of bubble sort. After
		// this pass, the largest element
		// is moved (or bubbled) to end.
		for (int i = 0; i < (n - 1); i++) {
			if (arr[i] > arr[i+1]) {
				// swap arr[i], arr[i+1]
				int temp = arr[i];
				arr[i] = arr[i+1];
				arr[i+1] = temp;
			}
		}

		// Largest element is fixed,
		// recur for remaining array
		recBubbleSort(arr, n-1);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/*
		Insertion sort is a simple sorting algorithm that works the
		way we sort playing cards in our hands.

		The idea behind Insertion Sort is dividing the array into the
		sorted and unsorted sub-arrays.

		Time Complexity: O(n*2) || O(n^2)
	 */
	private void insertionSort(int[] arr) {
		int n = arr.length;
		for (int i = 1; i < n; ++i) {
			int key = arr[i];
			int j = i - 1;

			/* Move elements of arr[0..i-1], that are
         greater than key, to one position ahead
         of their current position
      */
			while (j >= 0 && arr[j] > key) {
				arr[j + 1] = arr[j];
				j = j - 1;
			}
			arr[j + 1] = key;
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/*
	Merge Sort divides input array in two halves, calls itself for the
	two halves and then merges the two sorted halves. The merge() function
	is used for merging two halves. The merge(arr, l, m, r) is key process
	that assumes that arr[l..m] and arr[m+1..r] are sorted and merges the
	two sorted sub-arrays into one. See following C implementation for details.

	Time Complexity: T(n) = 2T(n/2) + O(nLogn)
	*/

	private void merge(int[] arr, int L, int M, int R) {
		// calculating lengths
		int lenL = M - L + 1;
		int lenR = R - M;

		// creating temporary sub-arrays
		int[] arrL = new int [lenL];
		int[] arrR = new int [lenR];

		// copying our sorted sub-arrays into temporaries
		for (int i = 0; i < lenL; i++) {
			arrL[i] = arr[L+i];
		}
		for (int i = 0; i < lenR; i++) {
			arrR[i] = arr[M+i+1];
		}

		// iterators containing current index of temp sub-arrays
		int idxL = 0;
		int idxR = 0;

		// copying from leftArray and rightArray back into array
		for (int i = L; i < R + 1; i++) {
			// if there are still un-copied elements in R and L, copy minimum of the two
			if (idxL < lenL && idxR < lenR) {
				if (arrL[idxL] < arrR[idxR]) {
					arr[i] = arrL[idxL];
					idxL++;
				}
				else {
					arr[i] = arrR[idxR];
					idxR++;
				}
			}
			// if all the elements have been copied from rightArray, copy the rest of leftArray
			else if (idxL < lenL) {
				arr[i] = arrL[idxL];
				idxL++;
			}
			// if all the elements have been copied from leftArray, copy the rest of rightArray
			else if (idxR < lenR) {
				arr[i] = arrR[idxR];
				idxR++;
			}
		}
	}

	// Main function that sorts arr[l..r] using
	// merge()
	private void mergeSort(int[] arr, int L, int R) {
		if (R <= L) return;
		int M = (L+R)/2;
		mergeSort(arr, L, M);
		mergeSort(arr, M+1, R);
		merge(arr, L, M, R);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/*
	A heap is a tree that satisfies the heap property,
	which is that for each node, all of its children
	are in a given relation to it. Additionally, a heap
	must be almost complete. An almost complete binary
	tree of depth d has a subtree of depth d-1 with the
	same root that is complete, and in which each node
	with a left descendant has a complete left subtree.
	In other words, when adding a node, we always go for
	the leftmost position in the lowest incomplete level.

	If the heap is a max-heap, then all of the children
	are smaller than the parent, and if it's a min-heap
	all of them are larger.

	Time Complexity: O(1) but really O(log n).
 */

	private void heapify(int[] arr, int len, int i) {
		int leftChild = 2*i+1;
		int rightChild = 2*i+2;
		int largest = i;

		// if the left child is larger than parent
		if (leftChild < len && arr[leftChild] > arr[largest]) {
			largest = leftChild;
		}

		// if the right child is larger than parent
		if (rightChild < len && arr[rightChild] > arr[largest]) {
			largest = rightChild;
		}

		// if a swap needs to occur
		if (largest != i) {
			int temp = arr[i];
			arr[i] = arr[largest];
			arr[largest] = temp;
			heapify(arr, len, largest);
		}
	}

	private void heapSort(int[] arr) {
		if (arr.length == 0) return;

		// Building the heap
		int len = arr.length;
		// we're going from the first non-leaf to the root
		for (int i = len / 2-1; i >= 0; i--)
			heapify(arr, len, i);

		for (int i = len-1; i >= 0; i--) {
			int tmp = arr[0];
			arr[0] = arr[i];
			arr[i] = tmp;

			heapify(arr, i, 0);
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/*
	Quick-sort is another Divide and Conquer algorithm.
	It picks one element of an array as the pivot and
	sorts all of the other elements around it, for
	example smaller elements to the left, and larger
	to the right.

	This guarantees that the pivot is in its proper
	place after the process. Then the algorithm
	recursively does the same for the left and right
	portions of the array.
	*/

	private int partition(int[] arr, int begin, int end) {
		int pivot = end;

		int counter = begin;
		for (int i = begin; i < end; i++) {
			if (arr[i] < arr[pivot]) {
				int temp = arr[counter];
				arr[counter] = arr[i];
				arr[i] = temp;
				counter++;
			}
		}
		int temp = arr[pivot];
		arr[pivot] = arr[counter];
		arr[counter] = temp;

		return counter;
	}

	private void quickSort(int[] array, int begin, int end) {
		if (end <= begin) return;
		int pivot = partition(array, begin, end);
		quickSort(array, begin, pivot-1);
		quickSort(array, pivot+1, end);
	}


	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ENTRY POINT

	public static void main(String[] args)
	{
		ArrayTools at = new ArrayTools();
		SortingAlgorithms ob = new SortingAlgorithms();

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// TEST BUBBLE SORT
		int max = 5000;
		int len = 750;

		final int[] refArray = at.random(max, len);
		int[] arr = Arrays.copyOf(refArray, refArray.length);

		System.out.println("Generate Initial Random Array Between 1 and " + max + " with an array size of " + len);
		at.print(arr);

		/////////////////////////////////////////////////////////////////////////////////////////////////////


		float startTime = Instant.now().getNano();
		ob.bubbleSort(arr);
		float stopTime = Instant.now().getNano();
		float elapsedTime = ((stopTime - startTime) / 1000000);
		System.out.println("\nBubble sorted array took " + elapsedTime + " milliseconds to execute");
		at.print(arr);

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// TEST INSERTION SORT
		arr = Arrays.copyOf(refArray, refArray.length);
		startTime = Instant.now().getNano();
		ob.insertionSort(arr);
		stopTime = Instant.now().getNano();
		elapsedTime = ((stopTime - startTime) / 1000000);
		System.out.println("\nInsertion sorted array took " + elapsedTime + " milliseconds to execute");
		at.print(arr);

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// TEST SELECTION SORT
		arr = Arrays.copyOf(refArray, refArray.length);
		startTime = Instant.now().getNano();
		ob.selectionSort(arr);
		stopTime = Instant.now().getNano();
		elapsedTime = ((stopTime - startTime) / 1000000);
		System.out.println("\nSelection sorted array took " + elapsedTime + " milliseconds to execute");
		at.print(arr);

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// TEST MERGE SORT
		arr = Arrays.copyOf(refArray, refArray.length);
		startTime = Instant.now().getNano();
		ob.mergeSort(arr, 0, (arr.length - 1));
		stopTime = Instant.now().getNano();
		elapsedTime = ((stopTime - startTime) / 1000000);
		System.out.println("\nMerge sorted array took " + elapsedTime + " milliseconds to execute");
		at.print(arr);

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// TEST HEAP SORT
		arr = Arrays.copyOf(refArray, refArray.length);
		startTime = Instant.now().getNano();
		ob.heapSort(arr);
		stopTime = Instant.now().getNano();
		elapsedTime = ((stopTime - startTime) / 1000000);
		System.out.println("\nHeap sorted array took " + elapsedTime + " milliseconds to execute");
		at.print(arr);

		/////////////////////////////////////////////////////////////////////////////////////////////////////
		// TEST QUICK SORT
		arr = Arrays.copyOf(refArray, refArray.length);
		startTime = Instant.now().getNano();
		ob.quickSort(arr, 0, (arr.length - 1));
		stopTime = Instant.now().getNano();
		elapsedTime = ((stopTime - startTime) / 1000000);
		System.out.println("\nQuick sorted array took " + elapsedTime + " milliseconds to execute");
		at.print(arr);
	}
}
