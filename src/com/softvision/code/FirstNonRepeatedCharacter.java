package com.softvision.code;

public class FirstNonRepeatedCharacter {

	private char firstNonRepeatedCharacter(String val) {
		String str = val.toLowerCase();
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (str.indexOf(c) == i && str.indexOf(c, i + 1) == -1) {
				return c;
			}
		}
		return ' ';
	}
	public static void main(String[] args) {
		String song = "Now let me tell you a story about a man name Jed. The poor mountaineer kept his family barely fed. Then one day, he was shooting at some food, and up through the ground come a bubblin crude.";
		song = song.trim();

		FirstNonRepeatedCharacter s = new FirstNonRepeatedCharacter();

		char a = s.firstNonRepeatedCharacter(song);

		System.out.println("The first non-repeating character is: " + a);
	}

}
