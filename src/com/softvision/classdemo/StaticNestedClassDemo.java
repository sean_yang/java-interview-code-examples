package com.softvision.classdemo;

class OuterClass {
	// static member
	private static int outer_x = 10;
	// private member
	private static int outer_private = 30;
	// instance(non-static) member
	int outer_y = 20;

	// static nested class
	static class StaticNestedClass {
		void display() {
			// can access static member of outer class
			System.out.println("outer_x = " + outer_x);

			// can access display private static member of outer class
			System.out.println("outer_private = " + outer_private);

			// The following statement will give compilation error
			// as static nested class cannot directly access non-static membera
			// System.out.println("outer_y = " + outer_y);

		}
	}

	class InnerClass {
		void display() {
			// can access static member of outer class
			System.out.println("outer_x = " + outer_x);

			// can also access non-static member of outer class
			System.out.println("outer_y = " + outer_y);

			// can also access private member of outer class
			System.out.println("outer_private = " + outer_private);
		}
	}
}

// Driver class 
public class StaticNestedClassDemo {
	public static void main(String[] args) {
		// accessing a static nested class
		OuterClass.StaticNestedClass nestedObject = new OuterClass.StaticNestedClass();

		OuterClass outerObject = new OuterClass();
		OuterClass.InnerClass innerObject = outerObject.new InnerClass();

		nestedObject.display();
		innerObject.display();
	}
} 