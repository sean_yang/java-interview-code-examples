package com.softvision.classdemo;

class Person {
	private String firstName = "";
	private String lastName = "";

	void setName(String first, String last) {
		this.firstName = first;
		this.lastName = last;
	}

	String getFullName() {
		return this.firstName + " " + this.lastName;
	}

	void copyValues(Person p) {
		this.firstName = p.firstName;
		this.lastName = p.lastName;
	}
}

public class PassByValOrRef {
	public static void main(String[] args) {
		Person p1 = new Person();
		Person p2 = new Person();
		p1.setName("John", "Smith");
		p2.setName("Billy", "Dean");

		System.out.println("Pre-swap is");
		System.out.println("P1 = " + p1.getFullName());
		System.out.println("P2 = " + p2.getFullName());

		swap(p1, p2);
		System.out.println("Post-swap is");
		System.out.println("P1 = " + p1.getFullName());
		System.out.println("P2 = " + p2.getFullName());


		updateName(p1, p2);
		System.out.println(p1.getFullName());
		copyPerson(p1);
		System.out.println(p1.getFullName());
	}

	private static void swap(Person rp1, Person rp2) {
		Person tmp = rp1;
		rp1 = rp2;
		rp1 = tmp;
	}

	private static void updateName(Person rp1, Person rp2) {
		rp1.setName("Jimmy", "Potts");
		rp2.setName("Jennie", "McBead");
	}

	private static void copyPerson(Person p) {
		Person p2 = new Person();
		p2.copyValues(p);
		p2.setName("Larry", "Bird");
	}
}
