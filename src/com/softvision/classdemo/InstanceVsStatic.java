package com.softvision.classdemo;

import com.softvision.models.Person;

public class InstanceVsStatic {
	public static void main(String[] args) {
		Person p1 = new Person();

		boolean staticVal = Person.isValidName(p1.getFullName());

		// Accessing a Class Level Method is allowed but not recommended.
		boolean instVal = Person.isValidName(p1.getFullName());

		System.out.println("The result of both is: " + staticVal + " | " + instVal);
	}
}
