package com.softvision.models;

public class Person {
	private String firstName;
	private String lastName;

	public Person() {
		this.firstName = "";
		this.lastName = "";
	}

	public Person(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}
		
		public void AddFirstName(String firstName) {
			this.firstName = firstName;
		}

		public void AddLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getFullName() {
			return this.firstName + " " + this.lastName;
		}
		
		public static boolean isValidName(String fullName) {
			return (fullName.contains(" "));
		}
	}