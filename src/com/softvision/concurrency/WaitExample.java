package com.softvision.concurrency;

public class WaitExample {
	public static void main(String[] args) {
		final PNBCustomer c = new PNBCustomer();
		// create two new thread and start
		// them simultaneously
		new Thread(() -> c.withdraw(15000)).start();
		new Thread(() -> c.deposit(10000)).start();
	}
}
