package com.softvision.concurrency;

public class JoinExample {
	public static void main(String[] args) {
		// creating two threads
		ThreadA a1 = new ThreadA();
		ThreadB b1 = new ThreadB();

		// starts second thread after when
		// first thread a1 is died.
		a1.start();
		try {
			a1.join();
		} catch (Exception e) {
			System.out.println(e);
		}

		// after thread a1 execution finished
		// then b1 thread start
		b1.start();
	}
}
