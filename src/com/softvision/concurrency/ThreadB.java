package com.softvision.concurrency;

class ThreadB extends Thread {

	@Override
	public void run() {
		for (char i = 'a'; i <= 'd'; i++) {
			try {
				Thread.sleep(100);
			} catch (Exception e) {
				System.out.println(e);
			}
			System.out.print(i + " ");
		}
	}
}