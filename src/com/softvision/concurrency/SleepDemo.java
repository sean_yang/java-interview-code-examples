package com.softvision.concurrency;

public class SleepDemo implements Runnable {

	public static void main(String[] args) throws Exception {
		Thread t1 = new Thread(new SleepDemo());
		Thread t2 = new Thread(new SleepDemo());
		t1.start();
		t2.start();
	}

	public void run() {
		for (int i = 0; i < 10; i++) {
			long s = (long) (Math.random() * 5000);
			System.out.println(Thread.currentThread().getName() + "  | Count: " + i + " | [sleep for " + s + " milliseconds]");
			try {
				// thread to sleep for 1000 milliseconds
				Thread.sleep(s);
			} catch (Exception ex) {
				System.out.println(ex.toString());
			}
		}
	}
}