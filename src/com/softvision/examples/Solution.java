package com.softvision.examples;

import java.util.*;
import java.lang.*;
import java.io.*;

/*
 * To execute Java, please define "static void main" on a class
 * named Solution.
 *
 * If you need more classes, simply define them inline.
 */

public class Solution {

	public static void main(String[] args) {
		ArrayList<String> strings = new ArrayList<String>();
		strings.add("Hello, World!");
		strings.add("Welcome to CoderPad.");
		strings.add("This pad is running Java 8.");

		for (String string : strings) {
			System.out.println(string);
		}

		int[] vals = {1,2,3,4};

		System.out.println("The products of " + Arrays.toString(vals) + " is " + Arrays.toString(GetProduct(vals)));
		String val = makePalindrome("aaesswdd");

		System.out.println("The Result is " + val);

		if (canFormPalindrome("geeksforgeeks"))
			System.out.println("Yes");
		else
			System.out.println("No");

		if (canFormPalindrome("geeksogeeks"))
			System.out.println("Yes");
		else
			System.out.println("No");

		String test = "abb";
		System.out.println(getPalindromeIfPresent(test));

		String test2a = "bbaa";
		System.out.println(getPalindromeIfPresent(test2a));

		String test2 = "nvreeodorevdne";
		System.out.println(getPalindromeIfPresent(test2));

		String test3 = "earth1earth";
		System.out.println(canPermutePalindrome1(test3));

	}

	private static int[] GetProduct(int[] vals){
		int[] products = new int[vals.length];

		for(int a = 0; a < vals.length; a++){
			int sum = 1;
			for(int b = 0; b < vals.length; b++){
				if(a != b){
					sum = sum * vals[b];
				}
			}
			products[a] = sum;
		}
		return products;
	}

	private static int getNumberSum(int number){
		int sum = 0;
		if(number == 0){
			return sum;
		} else {
			sum += (number%10);
			getNumberSum(number/10);
		}
		return sum;
	}

	private static boolean checkPalindrome(String s){

		for (int i = 0; i < s.length()/2; i++) {
			if (s.charAt(i) != s.charAt(s.length() - i - 1)) return false;
		}

		return true;

	}

	private static String makePalindrome(String base){
		String pref = "";
		int i = base.length() - 1;
		while(! checkPalindrome(pref + base)){
			pref = pref + base.charAt(i);
			i --;
		}
		return pref + base;
	}

	static int NO_OF_CHARS = 256;

	/* function to check whether characters
	of a string can form a palindrome */
	private static boolean canFormPalindrome(String str) {

		// Create a count array and initialize all
		// values as 0
		int count[] = new int[NO_OF_CHARS];
		Arrays.fill(count, 0);

		// For each character in input strings,
		// increment count in the corresponding
		// count array
		for (int i = 0; i < str.length(); i++)
			count[(int)(str.charAt(i))]++;

		// Count odd occurring characters
		int odd = 0;
		for (int i = 0; i < NO_OF_CHARS; i++)
		{
			if ((count[i] & 1) == 1)
				odd++;

			if (odd > 1)
				return false;
		}

		// Return true if odd count is 0 or 1,
		return true;
	}

	private static String getPalindromeIfPresent(String s) {

		// Basic validation
		if (s == null || s.length() < 2 || s.equals("") ) {
			return null;
		}
		Map<Character, Integer> mapChars = new HashMap<Character, Integer>();

		char[] chars = s.toCharArray();

		// Build map of each char and count occurrences in String
		for (int i = 0; i < chars.length; i++) {
			if (mapChars.containsKey(chars[i])) {
				Integer val = mapChars.get(chars[i]);
				mapChars.put(chars[i], val+1);
			} else {
				mapChars.put(chars[i], 1);
			}
		}

		// Validate String if possible to form palindrome rule is:
		// must have all chars count even, except middle char which can be odd
		int oneCharCount = 0;
		for (Map.Entry<Character, Integer> entry : mapChars.entrySet()) {
			if (entry.getValue() % 2 != 0) {
				oneCharCount++;

				if (oneCharCount > 1) {
					return null;
				}
			}
		}

		int sLen = s.length();
		int middle = sLen/2;
		Character midChar = null;
		StringBuilder s1 = new StringBuilder();

		// rearrange each group of chars distributing them at the start and end of string
		for (Map.Entry<Character, Integer> entry : mapChars.entrySet()) {
			int charCount = entry.getValue();
			Character c = entry.getKey();

			if (charCount % 2 != 0) {
				midChar = c;
			}

			int i = charCount / 2;
			while (i > 0) {
				s1.append(c);
				s1.insert(0, c);
				i--;
			}
		}

		// if middle Char exists from odd count, place it otherwise ignore
		if (midChar != null) {
			s1.insert(middle, midChar);
		}

		return s1.toString();
	}

	private static boolean canPermutePalindrome1(String s) {
		int count = 0;
		for (char i = 0; i < 128 && count <= 1; i++) {
			int ct = 0;
			for (int j = 0; j < s.length(); j++) {
				if (s.charAt(j) == i)
					ct++;
			}
			count += ct % 2;
		}
		return count <= 1;
	}

	private static boolean canPermutePalindrome2(String s) {
		HashMap < Character, Integer > map = new HashMap < > ();
		for (int i = 0; i < s.length(); i++) {
			map.put(s.charAt(i), map.getOrDefault(s.charAt(i), 0) + 1);
		}
		int count = 0;
		for (char key: map.keySet()) {
			count += map.get(key) % 2;
		}
		return count <= 1;
	}

	private static boolean canPermutePalindrome3(String s) {
		int[] map = new int[128];
		for (int i = 0; i < s.length(); i++) {
			map[s.charAt(i)]++;
		}
		int count = 0;
		for (int key = 0; key < map.length && count <= 1; key++) {
			count += map[key] % 2;
		}
		return count <= 1;
	}
}