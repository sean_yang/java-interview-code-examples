package com.softvision.examples;

public class Card implements Comparable<Card>
{
	private String suit;
	private int value;

	public Card(String suit, int value)
	{
		this.suit = suit;
		this.value = value;
	}

	public int getValue()
	{
		return this.value;
	}

	public String getSuit()
	{
		return this.suit;
	}

	public int compareTo(Card x)
	{
		return getValue() - x.getValue();
	}
}