package com.softvision.helpers;

import java.util.Arrays;
import java.util.Random;

public class ArrayTools {

	public void print(int[] arr) {
		Arrays.stream(arr).mapToObj(item -> item + " ").forEach(System.out::print);
		System.out.println();
	}

	public int[] random(int max, int len) {
		int[] arr = new int[len];
		int val = max - 1;
		Random rand = new Random();
		for (int i = 0; i < len; i++) {
			arr[i] = (rand.nextInt(val) + 1);
		}
		return arr;
	}
}
